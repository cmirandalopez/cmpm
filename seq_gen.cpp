#include <iostream>
#include <random>
#include <list>
#include <set>
#include <vector>
#include <cstdlib>

// argv[1] contains the number of sequences
// argv[2] contains the maximum length of a sequence
// argv[3] contains the length of an event
// argv[4] contains the maximum number of labels

using std::list;
using std::multiset;
using std::atoi;
using std::vector;
using std::ostream;

// Event structure (itemset, timestamp)
typedef struct event {
  multiset<int> itemset;
  int timestamp;
} t_event;

// Sequence class (list of events)
class sequence {

  public:
    sequence() {}
    sequence(int size, vector<int>& labels);
    static int item_len;
    friend ostream& operator<<(ostream& os, const sequence& seq);
  private:
    list<t_event> elements;
};

// Sequence constructor
sequence::sequence(int size, vector<int>& labels) : elements(size) {
  int tmp, k, i;
  vector<int> true_labels(sequence::item_len);
  i = 0;
  for (auto it = elements.begin(); it != elements.end(); ++it, ++i) {
    it->timestamp = i;
    tmp = 1;
    for (k = 0; k < sequence::item_len; ++k) {
      true_labels[k] = rand() % labels[k] + tmp;
      tmp += labels[k];
    }
    it->itemset.insert(true_labels.begin(), true_labels.end());
  } 
}

// Sequence printing
ostream& operator<<(ostream& os, const sequence& seq) {
  for (auto const& event : seq.elements) {
    os << "<" << event.timestamp << "> ";
    for (auto const& item : event.itemset) {
      os << item << " ";
    }
    os << "-1 ";
  }
  os << "0 -1 -2";
}

// Initialize static variable item_len
int sequence::item_len = 0;

int main(int argc, char *argv[]) {

  int nb_seq, max_seq_len, item_len, max_labels;
  nb_seq = atoi(argv[1]); max_seq_len = atoi(argv[2]); item_len = atoi(argv[3]);
  max_labels = atoi(argv[4]); sequence::item_len = item_len;

  // To determine which labels belong to which event type range
  int tmp_labels = max_labels;

  int i, j, tmp;
  vector<int> labels(item_len), true_labels(item_len);

  /*i = 0;
  while (i < item_len) {
    tmp = rand() % max_labels;
    if (2 * (item_len - i) == tmp_labels) {
      for (j = i; j < item_len; ++j) {
        labels[j] = 2;
      }
      i = item_len;
    } else if (2 * (tmp_labels - tmp) < item_len - i) {
      continue;
    } else {
      tmp_labels -= tmp;
      labels[i] = rand() % max_labels;
      i++;
    }
  }*/
  int lolz = max_labels / item_len;
  for (i = 0; i < item_len - 1; ++i) {
    labels[i] = lolz;
  }
  labels[item_len - 1] = max_labels - lolz * (item_len - 1);

  // Build the sequence database
  int seq_len;
  vector<sequence> sequences(nb_seq);

  for (i = 0; i < nb_seq; ++i) {
    seq_len = rand() % (max_seq_len - 1) + 2;
    sequences[i] = sequence(seq_len, labels);
  }

  // Print the sequence database
  std::cout << "# Number of sequences " << nb_seq << std::endl;
  std::cout << "# Max length for sequences " << max_seq_len << std::endl;
  std::cout << "# Itemset sizes " << item_len << std::endl;
  std::cout << "# Max number of labels " << max_labels << std::endl;

  for (auto const& seq : sequences) {
    std::cout << seq << std::endl;
  }
    
  return 0;
}
